```
mkdir -v artifacts
mkdir -v data
```

```
gitlab-runner exec docker \
  --docker-pull-policy="if-not-present" \
  --docker-volumes "$(pwd)/data:/var/lib/neo4j/data" \
  --docker-volumes "$(pwd)/artifacts:/tmp/artifacts" \
  neo4j:configure
```

```
gitlab-runner exec docker \
  --docker-pull-policy="if-not-present" \
  --docker-volumes "$(pwd)/data:/var/lib/neo4j/data" \
  --docker-volumes "$(pwd)/artifacts:/tmp/artifacts" \
  neo4j:create_db
```

```
gitlab-runner exec docker \
  --docker-pull-policy="if-not-present" \
  --docker-volumes "$(pwd)/data:/var/lib/neo4j/data" \
  --docker-volumes "$(pwd)/artifacts:/tmp/artifacts" \
  neo4j:export_db
```

```
rm -rf artifacts
rm -rf data
```
